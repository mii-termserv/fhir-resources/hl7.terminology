{
  "resourceType": "CodeSystem",
  "id": "hl7.term-v2-0734",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>grouperStatus</h2><tt>http://terminology.hl7.org/CodeSystem/v2-0734</tt><p>Code system of concepts specifying the status of a grouper in general.  US Realm. Used in HL7 Version 2.x messaging in the DRG segment.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-wg",
      "valueCode": "fm"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ],
  "url": "http://terminology.hl7.org/CodeSystem/v2-0734",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.18.393"
    }
  ],
  "version": "2.0.0",
  "name": "GrouperStatus",
  "title": "grouperStatus",
  "status": "active",
  "experimental": false,
  "date": "2019-12-01",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "Code system of concepts specifying the status of a grouper in general.  US Realm. Used in HL7 Version 2.x messaging in the DRG segment.",
  "purpose": "Underlying Master Code System for V2 table 0734 (Grouper Status)",
  "copyright": "This material derives from the HL7 Terminology (THO). THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "valueSet": "http://terminology.hl7.org/ValueSet/v2-0734",
  "hierarchyMeaning": "is-a",
  "compositional": false,
  "versionNeeded": false,
  "content": "complete",
  "property": [
    {
      "code": "status",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#status",
      "description": "Status of the concept",
      "type": "code"
    },
    {
      "code": "deprecated",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-table-deprecated",
      "description": "Version of HL7 in which the code was deprecated",
      "type": "code"
    }
  ],
  "concept": [
    {
      "id": "6430",
      "code": "0",
      "display": "Normal grouping",
      "definition": "Normal grouping",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6431",
      "code": "1",
      "display": "Invalid or missing primary diagnosis",
      "definition": "Invalid or missing primary diagnosis",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6432",
      "code": "2",
      "display": "Diagnosis is not allowed to be primary",
      "definition": "Diagnosis is not allowed to be primary",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6433",
      "code": "3",
      "display": "Data does not fulfill DRG criteria",
      "definition": "Data does not fulfill DRG criteria",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6434",
      "code": "4",
      "display": "Invalid age, admission date, date of birth or discharge date",
      "definition": "Invalid age, admission date, date of birth or discharge date",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6435",
      "code": "5",
      "display": "Invalid gender",
      "definition": "Invalid gender",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6436",
      "code": "6",
      "display": "Invalid discharge status",
      "definition": "Invalid discharge status",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6437",
      "code": "7",
      "display": "Invalid weight ad admission",
      "definition": "Invalid weight ad admission",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6438",
      "code": "8",
      "display": "Invalid length of stay",
      "definition": "Invalid length of stay",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6439",
      "code": "9",
      "display": "Invalid field \"same day\"",
      "definition": "Invalid field \"same day\"",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    }
  ],
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  }
}