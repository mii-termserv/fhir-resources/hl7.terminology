{
  "resourceType": "CodeSystem",
  "id": "hl7.term-data-absent-reason",
  "meta": {
    "lastUpdated": "2019-11-01T09:29:23.356+11:00",
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  },
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>DataAbsentReason</h2><tt>http://terminology.hl7.org/CodeSystem/data-absent-reason</tt><p>Used to specify why the normally expected content of the data element is missing.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-wg",
      "valueCode": "oo"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version",
      "valueCode": "4.0.0"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-fmm",
      "valueInteger": 5
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ],
  "url": "http://terminology.hl7.org/CodeSystem/data-absent-reason",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.4.642.4.1048"
    }
  ],
  "version": "1.0.0",
  "name": "DataAbsentReason",
  "title": "DataAbsentReason",
  "status": "active",
  "experimental": false,
  "date": "2019-11-01T09:29:23+11:00",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "Used to specify why the normally expected content of the data element is missing.",
  "copyright": "This material derives from the HL7 Terminology (THO). THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "valueSet": "http://hl7.org/fhir/ValueSet/data-absent-reason",
  "content": "complete",
  "concept": [
    {
      "code": "unknown",
      "display": "Unknown",
      "definition": "The value is expected to exist but is not known.",
      "concept": [
        {
          "code": "asked-unknown",
          "display": "Asked But Unknown",
          "definition": "The source was asked but does not know the value."
        },
        {
          "code": "temp-unknown",
          "display": "Temporarily Unknown",
          "definition": "There is reason to expect (from the workflow) that the value may become known."
        },
        {
          "code": "not-asked",
          "display": "Not Asked",
          "definition": "The workflow didn't lead to this value being known."
        },
        {
          "code": "asked-declined",
          "display": "Asked But Declined",
          "definition": "The source was asked but declined to answer."
        }
      ]
    },
    {
      "extension": [
        {
          "url": "http://hl7.org/fhir/StructureDefinition/codesystem-concept-comments",
          "valueString": "Using \"masked\" may be breach of security or confidentiality, but there are times        when its use is required to support alternate workflows for gaining access to denied information."
        }
      ],
      "code": "masked",
      "display": "Masked",
      "definition": "The information is not available due to security, privacy or related reasons."
    },
    {
      "code": "not-applicable",
      "display": "Not Applicable",
      "definition": "There is no proper value for this element (e.g. last menstrual period for a male)."
    },
    {
      "code": "unsupported",
      "display": "Unsupported",
      "definition": "The source system wasn't capable of supporting this element."
    },
    {
      "extension": [
        {
          "url": "http://hl7.org/fhir/StructureDefinition/codesystem-concept-comments",
          "valueString": "It may be linked by internal references (e.g. xml:id). This usually implies that the value        could not be represented in the correct format - this may be due to system limitations,        or this particular data value."
        }
      ],
      "code": "as-text",
      "display": "As Text",
      "definition": "The content of the data is represented in the resource narrative."
    },
    {
      "code": "error",
      "display": "Error",
      "definition": "Some system or workflow process error means that the information is not available.",
      "concept": [
        {
          "extension": [
            {
              "url": "http://hl7.org/fhir/StructureDefinition/codesystem-concept-comments",
              "valueString": "This is sometimes an output value from measuring devices."
            }
          ],
          "code": "not-a-number",
          "display": "Not a Number (NaN)",
          "definition": "The numeric value is undefined or unrepresentable due to a floating point processing error."
        },
        {
          "extension": [
            {
              "url": "http://hl7.org/fhir/StructureDefinition/codesystem-concept-comments",
              "valueString": "This is sometimes an output value from measuring devices."
            }
          ],
          "code": "negative-infinity",
          "display": "Negative Infinity (NINF)",
          "definition": "The numeric value is excessively low and unrepresentable due to a floating point processing        error."
        },
        {
          "extension": [
            {
              "url": "http://hl7.org/fhir/StructureDefinition/codesystem-concept-comments",
              "valueString": "This is sometimes an output value from measuring devices."
            }
          ],
          "code": "positive-infinity",
          "display": "Positive Infinity (PINF)",
          "definition": "The numeric value is excessively high and unrepresentable due to a floating point processing        error."
        }
      ]
    },
    {
      "code": "not-performed",
      "display": "Not Performed",
      "definition": "The value is not available because the observation procedure (test, etc.) was not performed."
    },
    {
      "extension": [
        {
          "url": "http://hl7.org/fhir/StructureDefinition/codesystem-concept-comments",
          "valueString": "This is most often associated with required bindings that do not include the actual code        used, but may be used with other data types."
        }
      ],
      "code": "not-permitted",
      "display": "Not Permitted",
      "definition": "The value is not permitted in this context (e.g. due to profiles, or the base data types)."
    }
  ]
}