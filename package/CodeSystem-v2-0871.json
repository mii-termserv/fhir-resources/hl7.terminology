{
  "resourceType": "CodeSystem",
  "id": "hl7.term-v2-0871",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>supplyRisk</h2><tt>http://terminology.hl7.org/CodeSystem/v2-0871</tt><p>Code system of concepts specifying any known or suspected hazard associated with this material item.  Used in HL7 Version 2.x messaging in the ITM segment.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-wg",
      "valueCode": "oo"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ],
  "url": "http://terminology.hl7.org/CodeSystem/v2-0871",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.18.410"
    }
  ],
  "version": "2.0.0",
  "name": "SupplyRisk",
  "title": "supplyRisk",
  "status": "active",
  "experimental": false,
  "date": "2019-12-01",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "Code system of concepts specifying any known or suspected hazard associated with this material item.  Used in HL7 Version 2.x messaging in the ITM segment.",
  "purpose": "Underlying Master Code System for V2 table 0871 (Supply Risk Codes)",
  "copyright": "This material derives from the HL7 Terminology (THO). THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "valueSet": "http://terminology.hl7.org/ValueSet/v2-0871",
  "hierarchyMeaning": "is-a",
  "compositional": false,
  "versionNeeded": false,
  "content": "complete",
  "property": [
    {
      "code": "status",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#status",
      "description": "Status of the concept",
      "type": "code"
    },
    {
      "code": "deprecated",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-table-deprecated",
      "description": "Version of HL7 in which the code was deprecated",
      "type": "code"
    },
    {
      "code": "v2-concComment",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-concComment",
      "description": "V2 Concept Comment",
      "type": "string"
    },
    {
      "code": "v2-concCommentAsPub",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-concCommentAsPub",
      "description": "V2 Concept Comment As Published",
      "type": "string"
    }
  ],
  "concept": [
    {
      "id": "6502",
      "code": "COR",
      "display": "Corrosive",
      "definition": "Corrosive",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Material is corrosive and may cause severe injury to skin, mucous membranes and eyes. Avoid any unprotected contact."
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Material is corrosive and may cause severe injury to skin, mucous membranes and eyes. Avoid any unprotected contact."
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6503",
      "code": "FLA",
      "display": "Flammable",
      "definition": "Flammable",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Material is highly flammable and in certain mixtures (with air) may lead to explosions.  Keep away from fire, sparks and excessive heat."
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Material is highly flammable and in certain mixtures (with air) may lead to explosions.  Keep away from fire, sparks and excessive heat."
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6504",
      "code": "EXP",
      "display": "Explosive",
      "definition": "Explosive",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Material is an explosive mixture.  Keep away from fire, sparks, and heat."
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Material is an explosive mixture.  Keep away from fire, sparks, and heat."
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6505",
      "code": "INJ",
      "display": "Injury Hazard",
      "definition": "Injury Hazard",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Material is solid and sharp (e.g., cannulas.)  Dispose in hard container."
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Material is solid and sharp (e.g., cannulas.)  Dispose in hard container."
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6506",
      "code": "TOX",
      "display": "Toxic",
      "definition": "Toxic",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Material is toxic to humans and/or animals.  Special care must be taken to avoid incorporation, even of small amounts."
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Material is toxic to humans and/or animals.  Special care must be taken to avoid incorporation, even of small amounts."
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6507",
      "code": "RAD",
      "display": "Radioactive",
      "definition": "Radioactive",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Material is a source for ionizing radiation and must be handled with special care to avoid injury of those who handle it and to avoid environmental hazards."
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Material is a source for ionizing radiation and must be handled with special care to avoid injury of those who handle it and to avoid environmental hazards."
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6508",
      "code": "UNK",
      "display": "Unknown",
      "definition": "Unknown",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Material hazard level is unknown."
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Material hazard level is unknown."
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    }
  ],
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  }
}