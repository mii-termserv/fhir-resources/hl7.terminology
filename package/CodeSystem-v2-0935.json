{
  "resourceType": "CodeSystem",
  "id": "hl7.term-v2-0935",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>processInterruptionReason</h2><tt>http://terminology.hl7.org/CodeSystem/v2-0935</tt><p>HL7-defined code system of concepts specifying the reason for the process interruption.  Used in HL7 Version 2.x messaging in the DON segment.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-wg",
      "valueCode": "oo"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ],
  "url": "http://terminology.hl7.org/CodeSystem/v2-0935",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.18.435"
    }
  ],
  "version": "2.0.0",
  "name": "ProcessInterruptionReason",
  "title": "processInterruptionReason",
  "status": "active",
  "experimental": false,
  "date": "2019-12-01",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "HL7-defined code system of concepts specifying the reason for the process interruption.  Used in HL7 Version 2.x messaging in the DON segment.",
  "purpose": "Underlying Master Code System for V2 table 0935 (Process Interruption Reason)",
  "copyright": "This material derives from the HL7 Terminology (THO). THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "valueSet": "http://terminology.hl7.org/ValueSet/v2-0935",
  "hierarchyMeaning": "is-a",
  "compositional": false,
  "versionNeeded": false,
  "content": "complete",
  "property": [
    {
      "code": "status",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#status",
      "description": "Status of the concept",
      "type": "code"
    },
    {
      "code": "deprecated",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-table-deprecated",
      "description": "Version of HL7 in which the code was deprecated",
      "type": "code"
    },
    {
      "code": "v2-concComment",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-concComment",
      "description": "V2 Concept Comment",
      "type": "string"
    },
    {
      "code": "v2-concCommentAsPub",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-concCommentAsPub",
      "description": "V2 Concept Comment As Published",
      "type": "string"
    }
  ],
  "concept": [
    {
      "id": "6696",
      "code": "NRG",
      "display": "No reason given, donor decided to stop without giving a reason",
      "definition": "No reason given, donor decided to stop without giving a reason",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6697",
      "code": "PCD",
      "display": "Phone Call-Donor",
      "definition": "Phone Call-Donor",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6698",
      "code": "DCW",
      "display": "Couldn't wait",
      "definition": "Couldn't wait",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6699",
      "code": "CFT",
      "display": "Couldn't follow through with donation (scared)",
      "definition": "Couldn't follow through with donation (scared)",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6700",
      "code": "DBB",
      "display": "Bathroom",
      "definition": "Bathroom",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6701",
      "code": "DNI",
      "display": "Phlebotomy Issue",
      "definition": "Phlebotomy Issue",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6702",
      "code": "ASC",
      "display": "Apheresis Software Crash",
      "definition": "Apheresis Software Crash",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6703",
      "code": "BSC",
      "display": "Manufacturing Software Crash",
      "definition": "Manufacturing Software Crash",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6704",
      "code": "GFE",
      "display": "General Facility Emergency",
      "definition": "General Facility Emergency",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Power outage,  natural disaster (tornado, flood, hurricane, etc.), air conditioning failure, etc."
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Power outage,  natural disaster (tornado, flood, hurricane, etc.), air conditioning failure, etc."
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    }
  ],
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  }
}