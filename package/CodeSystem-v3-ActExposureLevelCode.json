{
  "resourceType": "CodeSystem",
  "id": "hl7.term-v3-ActExposureLevelCode",
  "language": "en",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>ActExposureLevelCode</h2><tt>http://terminology.hl7.org/CodeSystem/v3-ActExposureLevelCode</tt><p>A qualitative measure of the degree of exposure to the causative agent. This includes concepts such as &quot;low&quot;, &quot;medium&quot; and &quot;high&quot;. This quantifies how the quantity that was available to be administered to the target differs from typical or background levels of the substance.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "url": "http://terminology.hl7.org/CodeSystem/v3-ActExposureLevelCode",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.5.1114"
    }
  ],
  "version": "3.0.0",
  "name": "ActExposureLevelCode",
  "title": "ActExposureLevelCode",
  "status": "active",
  "experimental": false,
  "date": "2019-03-20",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "A qualitative measure of the degree of exposure to the causative agent. This includes concepts such as \"low\", \"medium\" and \"high\". This quantifies how the quantity that was available to be administered to the target differs from typical or background levels of the substance.",
  "copyright": "This material derives from the HL7 Terminology THO. THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "hierarchyMeaning": "is-a",
  "content": "complete",
  "property": [
    {
      "extension": [
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-symmetry",
          "valueCode": "antisymmetric"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-transitivity",
          "valueCode": "transitive"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-reflexivity",
          "valueCode": "irreflexive"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-isNavigable",
          "valueBoolean": true
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-relationshipKind",
          "valueCode": "Specializes"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-inverseName",
          "valueString": "Generalizes"
        }
      ],
      "code": "Specializes",
      "description": "The child code is a more narrow version of the concept represented by the parent code.  I.e. Every child concept is also a valid parent concept.  Used to allow determination of subsumption.  Must be transitive, irreflexive, antisymmetric.",
      "type": "Coding"
    },
    {
      "extension": [
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-symmetry",
          "valueCode": "antisymmetric"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-transitivity",
          "valueCode": "transitive"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-reflexivity",
          "valueCode": "irreflexive"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-isNavigable",
          "valueBoolean": true
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-relationshipKind",
          "valueCode": "Generalizes"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-inverseName",
          "valueString": "Specializes"
        }
      ],
      "code": "Generalizes",
      "description": "Inverse of Specializes.  Only included as a derived relationship.",
      "type": "Coding"
    },
    {
      "code": "internalId",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v3-internal-id",
      "description": "The internal identifier for the concept in the HL7 Access database repository.",
      "type": "code"
    },
    {
      "code": "notSelectable",
      "uri": "http://hl7.org/fhir/concept-properties#notSelectable",
      "description": "Indicates that the code is abstract - only intended to be used as a selector for other concepts",
      "type": "boolean"
    },
    {
      "code": "status",
      "uri": "http://hl7.org/fhir/concept-properties#status",
      "description": "Designation of a concept's state. Normally is not populated unless the state is retired.",
      "type": "code"
    }
  ],
  "concept": [
    {
      "code": "_ActExposureLevelCode",
      "display": "ActExposureLevelCode",
      "definition": "A qualitative measure of the degree of exposure to the causative agent. This includes concepts such as \"low\", \"medium\" and \"high\". This quantifies how the quantity that was available to be administered to the target differs from typical or background levels of the substance.",
      "property": [
        {
          "code": "notSelectable",
          "valueBoolean": true
        },
        {
          "code": "status",
          "valueCode": "active"
        },
        {
          "code": "internalId",
          "valueCode": "22372"
        }
      ],
      "concept": [
        {
          "code": "HIGH",
          "display": "high",
          "definition": "**Description:** Exposure to an agent at a relatively high level above background.",
          "property": [
            {
              "code": "status",
              "valueCode": "active"
            },
            {
              "code": "internalId",
              "valueCode": "22373"
            }
          ]
        },
        {
          "code": "LOW",
          "display": "low",
          "definition": "**Description:** Exposure to an agent at a relatively low level above background.",
          "property": [
            {
              "code": "status",
              "valueCode": "active"
            },
            {
              "code": "internalId",
              "valueCode": "22374"
            }
          ]
        },
        {
          "code": "MEDIUM",
          "display": "medium",
          "definition": "**Description:** Exposure to an agent at a relatively moderate level above background.A",
          "property": [
            {
              "code": "status",
              "valueCode": "active"
            },
            {
              "code": "internalId",
              "valueCode": "22375"
            }
          ]
        }
      ]
    }
  ],
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ]
}