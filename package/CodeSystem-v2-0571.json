{
  "resourceType": "CodeSystem",
  "id": "hl7.term-v2-0571",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>invoiceProcessingResultsStatus</h2><tt>http://terminology.hl7.org/CodeSystem/v2-0571</tt><p>Code system of concepts used to specify the processing status for an Invoice Processing Result.  Used in the Invoice Processing Result (IPR) segment in HL7 Version 2.x messaging.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-wg",
      "valueCode": "fm"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ],
  "url": "http://terminology.hl7.org/CodeSystem/v2-0571",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.18.371"
    }
  ],
  "version": "2.0.0",
  "name": "InvoiceProcessingResultsStatus",
  "title": "invoiceProcessingResultsStatus",
  "status": "active",
  "experimental": false,
  "date": "2019-12-01",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "Code system of concepts used to specify the processing status for an Invoice Processing Result.  Used in the Invoice Processing Result (IPR) segment in HL7 Version 2.x messaging.",
  "purpose": "Underlying Master Code System for V2 table 0571 (Invoice Processing Results Status)",
  "copyright": "This material derives from the HL7 Terminology (THO). THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "valueSet": "http://terminology.hl7.org/ValueSet/v2-0571",
  "hierarchyMeaning": "is-a",
  "compositional": false,
  "versionNeeded": false,
  "content": "complete",
  "property": [
    {
      "code": "status",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#status",
      "description": "Status of the concept",
      "type": "code"
    },
    {
      "code": "deprecated",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-table-deprecated",
      "description": "Version of HL7 in which the code was deprecated",
      "type": "code"
    },
    {
      "code": "v2-concComment",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-concComment",
      "description": "V2 Concept Comment",
      "type": "string"
    },
    {
      "code": "v2-concCommentAsPub",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-concCommentAsPub",
      "description": "V2 Concept Comment As Published",
      "type": "string"
    }
  ],
  "concept": [
    {
      "id": "6243",
      "code": "ACK",
      "display": "Acknowledge",
      "definition": "Acknowledge",
      "designation": [
        {
          "language": "de",
          "use": {
            "system": "http://terminology.hl7.org/CodeSystem/hl7TermMaintInfra",
            "code": "preferredForLanguage"
          },
          "value": "Empfangsbestätigung"
        }
      ],
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6244",
      "code": "REJECT",
      "display": "Reject",
      "definition": "Reject",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6245",
      "code": "PEND",
      "display": "Pending",
      "definition": "Pending",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6246",
      "code": "ADJZER",
      "display": "Adjudicated to Zero",
      "definition": "Adjudicated to Zero",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6247",
      "code": "ADJSUB",
      "display": "Adjudicated as Submitted",
      "definition": "Adjudicated as Submitted",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6248",
      "code": "ADJ",
      "display": "Adjudicated with Adjustments",
      "definition": "Adjudicated with Adjustments",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6249",
      "code": "PAID",
      "display": "Paid",
      "definition": "Paid",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "6250",
      "code": "PRED",
      "display": "Pre-Determination",
      "definition": "Pre-Determination",
      "property": [
        {
          "code": "v2-concComment",
          "valueString": "Indicates that the IPR has been adjudicated but will not be paid.  Equivalent to ADJUD (Adjudicate)"
        },
        {
          "code": "v2-concCommentAsPub",
          "valueString": "Indicates that the IPR has been adjudicated but will not be paid.  Equivalent to ADJUD (Adjudicate)"
        },
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    }
  ],
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  }
}