{
  "resourceType": "CodeSystem",
  "id": "hl7.term-v3-ActInvoiceElementModifier",
  "language": "en",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>ActInvoiceElementModifier</h2><tt>http://terminology.hl7.org/CodeSystem/v3-ActInvoiceElementModifier</tt><p>Processing consideration and clarification codes.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "url": "http://terminology.hl7.org/CodeSystem/v3-ActInvoiceElementModifier",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.5.1051"
    }
  ],
  "version": "3.0.0",
  "name": "ActInvoiceElementModifier",
  "title": "ActInvoiceElementModifier",
  "status": "active",
  "experimental": false,
  "date": "2019-03-20",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "Processing consideration and clarification codes.",
  "copyright": "This material derives from the HL7 Terminology THO. THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "hierarchyMeaning": "is-a",
  "content": "complete",
  "property": [
    {
      "extension": [
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-symmetry",
          "valueCode": "antisymmetric"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-transitivity",
          "valueCode": "transitive"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-reflexivity",
          "valueCode": "irreflexive"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-isNavigable",
          "valueBoolean": true
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-relationshipKind",
          "valueCode": "Specializes"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-inverseName",
          "valueString": "Generalizes"
        }
      ],
      "code": "Specializes",
      "description": "The child code is a more narrow version of the concept represented by the parent code.  I.e. Every child concept is also a valid parent concept.  Used to allow determination of subsumption.  Must be transitive, irreflexive, antisymmetric.",
      "type": "Coding"
    },
    {
      "extension": [
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-symmetry",
          "valueCode": "antisymmetric"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-transitivity",
          "valueCode": "transitive"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-reflexivity",
          "valueCode": "irreflexive"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-isNavigable",
          "valueBoolean": true
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-relationshipKind",
          "valueCode": "Generalizes"
        },
        {
          "url": "http://terminology.hl7.org/StructureDefinition/ext-mif-relationship-inverseName",
          "valueString": "Specializes"
        }
      ],
      "code": "Generalizes",
      "description": "Inverse of Specializes.  Only included as a derived relationship.",
      "type": "Coding"
    },
    {
      "code": "internalId",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v3-internal-id",
      "description": "The internal identifier for the concept in the HL7 Access database repository.",
      "type": "code"
    },
    {
      "code": "status",
      "uri": "http://hl7.org/fhir/concept-properties#status",
      "description": "Designation of a concept's state. Normally is not populated unless the state is retired.",
      "type": "code"
    }
  ],
  "concept": [
    {
      "code": "EFORM",
      "display": "Electronic Form To Follow",
      "definition": "Electronic form with supporting information to follow.",
      "property": [
        {
          "code": "status",
          "valueCode": "active"
        },
        {
          "code": "internalId",
          "valueCode": "17706"
        }
      ]
    },
    {
      "code": "FAX",
      "display": "Fax To Follow",
      "definition": "Fax with supporting information to follow.",
      "property": [
        {
          "code": "status",
          "valueCode": "active"
        },
        {
          "code": "internalId",
          "valueCode": "17707"
        }
      ]
    },
    {
      "code": "LINV",
      "display": "Last Invoice",
      "definition": "Represents the last invoice from the perspective of the provider.",
      "property": [
        {
          "code": "status",
          "valueCode": "active"
        },
        {
          "code": "internalId",
          "valueCode": "17708"
        }
      ]
    },
    {
      "code": "PAPER",
      "display": "Paper Documentation To Follow",
      "definition": "Paper documentation (or other physical format) with supporting information to follow.",
      "property": [
        {
          "code": "status",
          "valueCode": "active"
        },
        {
          "code": "internalId",
          "valueCode": "17705"
        }
      ]
    }
  ],
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ]
}