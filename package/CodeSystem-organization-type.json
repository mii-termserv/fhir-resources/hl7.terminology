{
  "resourceType": "CodeSystem",
  "id": "hl7.term-organization-type",
  "meta": {
    "lastUpdated": "2020-04-09T21:10:28.568+00:00",
    "profile": [
      "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
    ],
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  },
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>Organization type</h2><tt>http://terminology.hl7.org/CodeSystem/organization-type</tt><p>This example value set defines a set of codes that can be used to indicate a type of organization.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-wg",
      "valueCode": "pa"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-fmm",
      "valueInteger": 1
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ],
  "url": "http://terminology.hl7.org/CodeSystem/organization-type",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.4.642.1.1128"
    }
  ],
  "version": "2.0.0",
  "name": "OrganizationType",
  "title": "Organization type",
  "status": "draft",
  "experimental": false,
  "date": "2024-03-09T14:36:19-07:00",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "This example value set defines a set of codes that can be used to indicate a type of organization.",
  "copyright": "This material derives from the HL7 Terminology (THO). THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "valueSet": "http://terminology.hl7.org/ValueSet/organization-type",
  "content": "complete",
  "concept": [
    {
      "code": "prov",
      "display": "Healthcare Provider",
      "definition": "An organization that provides healthcare services."
    },
    {
      "code": "dept",
      "display": "Hospital Department",
      "definition": "A department or ward within a hospital (Generally is not applicable to top level organizations)"
    },
    {
      "code": "team",
      "display": "Organizational team",
      "definition": "An organizational team is usually a grouping of practitioners that perform a specific function within an organization (which could be a top level organization, or a department)."
    },
    {
      "code": "govt",
      "display": "Government",
      "definition": "A political body, often used when including organization records for government bodies such as a Federal Government, State or Local Government."
    },
    {
      "code": "ins",
      "display": "Insurance Company",
      "definition": "A company that provides insurance to its subscribers that may include healthcare related policies."
    },
    {
      "code": "pay",
      "display": "Payer",
      "definition": "A company, charity, or governmental organization, which processes claims and/or issues payments to providers on behalf of patients or groups of patients."
    },
    {
      "code": "edu",
      "display": "Educational Institute",
      "definition": "An educational institution that provides education or research facilities."
    },
    {
      "code": "reli",
      "display": "Religious Institution",
      "definition": "An organization that is identified as a part of a religious institution."
    },
    {
      "code": "crs",
      "display": "Clinical Research Sponsor",
      "definition": "An organization that is identified as a Pharmaceutical/Clinical Research Sponsor."
    },
    {
      "code": "cg",
      "display": "Community Group",
      "definition": "An un-incorporated community group."
    },
    {
      "code": "bus",
      "display": "Non-Healthcare Business or Corporation",
      "definition": "An organization that is a registered business or corporation but not identified by other types."
    },
    {
      "code": "other",
      "display": "Other",
      "definition": "Other type of organization not already specified."
    },
    {
      "code": "laboratory",
      "display": "Laboratory",
      "definition": "An organization that conducts medical tests."
    },
    {
      "code": "imaging",
      "display": "Imaging Center",
      "definition": "An organization specialized in providing diagnostic imaging services."
    },
    {
      "code": "pharmacy",
      "display": "Pharmacy",
      "definition": "An organization focused on dispensing medications and offering pharmaceutical care."
    },
    {
      "code": "health-information-network",
      "display": "Health Information Network",
      "definition": "An organization focused on enabling the exchange and integration of health information among healthcare entities."
    },
    {
      "code": "health-data-aggregator",
      "display": "Health Data Aggregator",
      "definition": "An organization focused on compiling health-related data from various sources."
    }
  ]
}