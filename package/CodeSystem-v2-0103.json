{
  "resourceType": "CodeSystem",
  "id": "hl7.term-v2-0103",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>processingId</h2><tt>http://terminology.hl7.org/CodeSystem/v2-0103</tt><p>HL7-defined code system of concepts which specify whether the message is part of a production, training or debugging system.  Used in HL7 Version 2.x messaging in the PT datatype.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-wg",
      "valueCode": "inm"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ],
  "url": "http://terminology.hl7.org/CodeSystem/v2-0103",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.18.40"
    }
  ],
  "version": "2.0.0",
  "name": "ProcessingId",
  "title": "processingId",
  "status": "active",
  "experimental": false,
  "date": "2019-12-01",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "HL7-defined code system of concepts which specify whether the message is part of a production, training or debugging system.  Used in HL7 Version 2.x messaging in the PT datatype.",
  "purpose": "Underlying Master Code System for V2 table 0103 (Processing ID)",
  "copyright": "This material derives from the HL7 Terminology (THO). THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "valueSet": "http://terminology.hl7.org/ValueSet/v2-0103",
  "hierarchyMeaning": "is-a",
  "compositional": false,
  "versionNeeded": false,
  "content": "complete",
  "property": [
    {
      "code": "status",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#status",
      "description": "Status of the concept",
      "type": "code"
    },
    {
      "code": "deprecated",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-table-deprecated",
      "description": "Version of HL7 in which the code was deprecated",
      "type": "code"
    }
  ],
  "concept": [
    {
      "id": "1316",
      "code": "D",
      "display": "Debugging",
      "definition": "Messages used for identification and correction of software errors.",
      "designation": [
        {
          "language": "de",
          "use": {
            "system": "http://terminology.hl7.org/CodeSystem/hl7TermMaintInfra",
            "code": "preferredForLanguage"
          },
          "value": "Test"
        }
      ],
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "1317",
      "code": "P",
      "display": "Production",
      "definition": "Messages used for communication of live production data.",
      "designation": [
        {
          "language": "de",
          "use": {
            "system": "http://terminology.hl7.org/CodeSystem/hl7TermMaintInfra",
            "code": "preferredForLanguage"
          },
          "value": "Routine / Produktion"
        }
      ],
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "1318",
      "code": "T",
      "display": "Training",
      "definition": "Messages used for training, where new/updated configurations are utilized to prepare users outside of a production setting.",
      "designation": [
        {
          "language": "de",
          "use": {
            "system": "http://terminology.hl7.org/CodeSystem/hl7TermMaintInfra",
            "code": "preferredForLanguage"
          },
          "value": "Schulung"
        }
      ],
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "1319",
      "code": "N",
      "display": "Non-Production Testing",
      "definition": "Messages used for testing of an interface for structure, content, and conformance between trading partners, using non-production data.",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "1320",
      "code": "V",
      "display": "Validation",
      "definition": "Messages used for conformance testing by a third party; for example, as part of certification.",
      "designation": [
        {
          "language": "de",
          "use": {
            "system": "http://terminology.hl7.org/CodeSystem/hl7TermMaintInfra",
            "code": "preferredForLanguage"
          },
          "value": "Validierung"
        }
      ],
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    }
  ],
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  }
}