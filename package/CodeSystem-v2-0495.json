{
  "resourceType": "CodeSystem",
  "id": "hl7.term-v2-0495",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>bodySiteModifier</h2><tt>http://terminology.hl7.org/CodeSystem/v2-0495</tt><p>HL7-defined code system of concepts specifying the modifier for the body site.  Used in HL7 Version 2.x messaging in the RXR segment.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>HL7 International</td></tr><tr><td><b>Dataset</b></td><td>HL7 International</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#tho\">THO license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/hl7.terminology\"><code>@mii-termserv/hl7.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/structuredefinition-wg",
      "valueCode": "oo"
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/hl7.terminology"
    }
  ],
  "url": "http://terminology.hl7.org/CodeSystem/v2-0495",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:2.16.840.1.113883.18.319"
    }
  ],
  "version": "2.0.0",
  "name": "BodySiteModifier",
  "title": "bodySiteModifier",
  "status": "active",
  "experimental": false,
  "date": "2019-12-01",
  "publisher": "Health Level Seven International",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://hl7.org"
        },
        {
          "system": "email",
          "value": "hq@HL7.org"
        }
      ]
    }
  ],
  "description": "HL7-defined code system of concepts specifying the modifier for the body site.  Used in HL7 Version 2.x messaging in the RXR segment.",
  "purpose": "Underlying Master Code System for V2 table 0495 (Body Site Modifier)",
  "copyright": "This material derives from the HL7 Terminology (THO). THO is copyright ©1989+ Health Level Seven International and is made available under the CC0 designation. For more licensing information see: https://terminology.hl7.org/license",
  "caseSensitive": true,
  "valueSet": "http://terminology.hl7.org/ValueSet/v2-0495",
  "hierarchyMeaning": "is-a",
  "compositional": false,
  "versionNeeded": false,
  "content": "complete",
  "property": [
    {
      "code": "status",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#status",
      "description": "Status of the concept",
      "type": "code"
    },
    {
      "code": "deprecated",
      "uri": "http://terminology.hl7.org/CodeSystem/utg-concept-properties#v2-table-deprecated",
      "description": "Version of HL7 in which the code was deprecated",
      "type": "code"
    }
  ],
  "concept": [
    {
      "id": "5338",
      "code": "ANT",
      "display": "Anterior",
      "definition": "Anterior",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5339",
      "code": "BIL",
      "display": "Bilateral",
      "definition": "Bilateral",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5340",
      "code": "DIS",
      "display": "Distal",
      "definition": "Distal",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5341",
      "code": "EXT",
      "display": "External",
      "definition": "External",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5342",
      "code": "LAT",
      "display": "Lateral",
      "definition": "Lateral",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5343",
      "code": "L",
      "display": "Left",
      "definition": "Left",
      "designation": [
        {
          "language": "de",
          "use": {
            "system": "http://terminology.hl7.org/CodeSystem/hl7TermMaintInfra",
            "code": "preferredForLanguage"
          },
          "value": "links"
        }
      ],
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5344",
      "code": "LOW",
      "display": "Lower",
      "definition": "Lower",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5345",
      "code": "MED",
      "display": "Medial",
      "definition": "Medial",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5346",
      "code": "POS",
      "display": "Posterior",
      "definition": "Posterior",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5347",
      "code": "PRO",
      "display": "Proximal",
      "definition": "Proximal",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5348",
      "code": "LLQ",
      "display": "Quadrant, Left Lower",
      "definition": "Quadrant, Left Lower",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5349",
      "code": "LUQ",
      "display": "Quadrant, Left Upper",
      "definition": "Quadrant, Left Upper",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5350",
      "code": "RLQ",
      "display": "Quadrant, Right Lower",
      "definition": "Quadrant, Right Lower",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5351",
      "code": "RUQ",
      "display": "Quadrant, Right Upper",
      "definition": "Quadrant, Right Upper",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5352",
      "code": "R",
      "display": "Right",
      "definition": "Right",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    },
    {
      "id": "5353",
      "code": "UPP",
      "display": "Upper",
      "definition": "Upper",
      "property": [
        {
          "code": "status",
          "valueCode": "A"
        }
      ]
    }
  ],
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "hl7",
        "display": "HL7 International"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#tho",
        "display": "THO license"
      }
    ]
  }
}