#!/bin/bash

files=$(find ./package -name "*.json" -not -name "package.json" -not -name ".index.json" | sort)
cat /dev/null >| package/.index.ndjson.tmp
for filename in $files; do
  resource_data=$(jq -rc '{resourceType, id, url, version}' $filename)
  resource_type=$(echo $resource_data | jq -rc '.resourceType')
  id=$(echo $resource_data | jq -rc '.id')
  url=$(echo $resource_data | jq -rc '.url' $filename)
  version=$(echo $resource_data | jq -rc '.version' $filename)

  json_string=$(jq -nc --arg file_name "$filename" --arg resource_type "$resource_type" --arg id "$id" --arg url "$url" --arg version "$version" '{filename: $file_name, resourceType: $resource_type, id: $id, url: $url, version: $version}')
  echo "$json_string" >> package/.index.ndjson.tmp
done

index_version=$(date +'%Y%m%d%H%M')
jq -c --slurp '.' package/.index.ndjson.tmp | jq --arg INDEX_VERSION $index_version '{"index-version": ($INDEX_VERSION | tonumber), "files": .}' > package/.index.json
rm package/.index.ndjson.tmp
ls -l package/.index.json
