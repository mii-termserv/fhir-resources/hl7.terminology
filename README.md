# HL7 Terminology

This package repackages the HL7 Terminology from https://terminology.hl7.org.

CodeSystems are only those with `content=complete`, i.e. no placeholder CS are included in this redistribution.

## Missing CodeSystems

If a ValueSet is defined that solely includes a CS listed below, it will not be included in this list.

### `content=not-present`

- `urn:iso:std:iso:3166` (ISO country codes, see https://terminology.hl7.org/5.5.0/ISO3166.html)
- `urn:iso:std:iso:3166:-2` (see above)
- http://nucc.org/provider-taxonomy
- http://terminology.hl7.org/CodeSystem/csaid
- https://terminology.hl7.org/5.5.0/CodeSystem-hsloc.html
- https://terminology.hl7.org/5.5.0/CodeSystem-CMSPlaceofServiceCodes.html
- http://terminology.hl7.org/CodeSystem/EPSG-GeodeticParameterDataset
- http://terminology.hl7.org/CodeSystem/epsg-ca
- http://terminology.hl7.org/CodeSystem/SCDHEC-GISSpatialAccuracyTiers
- http://terminology.hl7.org/CodeSystem/v2-0350
- http://terminology.hl7.org/CodeSystem/v2-0351
- http://terminology.hl7.org/CodeSystem/v2-0456
- http://terminology.hl7.org/CodeSystem/v2-0895
- http://terminology.hl7.org/CodeSystem/nubc-UB92
- http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl
- http://terminology.hl7.org/CodeSystem/icd9cm
- https://www.nlm.nih.gov/mesh
- http://www.cms.gov/Medicare/Coding/HCPCSReleaseCodeSets
- http://terminology.hl7.org/CodeSystem/naics
- http://terminology.hl7.org/ValueSet/cpt-all
- http://hl7.org/fhir/sid/mvx
- http://cihi.ca/fhir/CodeSystem/cihi-cci
- http://hl7.org/fhir/sid/cvx

### `content=fragment`

- http://terminology.hl7.org/CodeSystem/triggerEventID
- http://www.ada.org/snodent
- http://terminology.hl7.org/CodeSystem/insurance-plan-type

### Other CodeSystems

- http://unitsofmeasure.org (can't be enumerated as a CodeSystem)
- http://nucc.org/provider-taxonomy (could be converted to a CodeSystem, but has no relevance outside of the United States)
- http://ncithesaurus-stage.nci.nih.gov (could maybe be converted to a CodeSystem, but of unclear relevance)
- http://terminology.hl7.org/CodeSystem/ietf3066 (no metada record, protected content?)

#### retired CodeSystems that are present, but have empty definition

- http://terminology.hl7.org/CodeSystem/v3-iso4217-HL7
- http://terminology.hl7.org/CodeSystem/v3-Country
- http://terminology.hl7.org/CodeSystem/v3-EncounterAccident
- http://terminology.hl7.org/CodeSystem/v3-ISO3166-1retired
- http://terminology.hl7.org/CodeSystem/v3-ISO3166-2retired
- http://terminology.hl7.org/CodeSystem/v3-ISO3166-3retired
- http://terminology.hl7.org/CodeSystem/v3-MaterialType
- http://terminology.hl7.org/CodeSystem/v3-MaterialForm
- http://terminology.hl7.org/CodeSystem/v3-EncounterAcuity

## ValueSets that don't expand

- http://terminology.hl7.org/ValueSet/v3-ActInvoiceDetailClinicalProductCode (see https://jira.hl7.org/browse/UP-566, but not relevant for SU-TermServ currently)
    - also http://terminology.hl7.org/ValueSet/v3-ActClassCompositeOrder
    - also http://terminology.hl7.org/ValueSet/v3-RoleClassSubstancePresence
    - also https://terminology.hl7.org/5.5.0/ValueSet-v2-0338.html
- http://terminology.hl7.org/ValueSet/snomed-intl-gps (this requires a specific version of SNOMED CT that's currently not indexed, see https://gitlab.com/mii-termserv/fhir-resources/hl7.terminology/-/issues/1)
- VS referencing SNODENT
- http://terminology.hl7.org/ValueSet/v2-0005 (missing CDCREC, not relevant for DE)
- http://terminology.hl7.org/ValueSet/v3-ActRelationshipTemporallyPertainsEnd and http://terminology.hl7.org/ValueSet/v3-ActRelationshipTemporallyPertainsStart (definition has no target concepts)
- http://terminology.hl7.org/ValueSet/v2-0719 (references wrong version of CS, but VS is retired and thus not a problem)
- http://terminology.hl7.org/ValueSet/v3-HL7FormatCodes (will be added, see http://terminology.hl7.org/ValueSet/v3-HL7FormatCodes)
- http://terminology.hl7.org/ValueSet/v3-GregorianCalendarCycle (potential definition issue in the CS, see https://jira.hl7.org/projects/UP/issues/UP-568)
- http://terminology.hl7.org/ValueSet/digital-certificate (the CS is actually missing, see https://jira.hl7.org/browse/UP-569)